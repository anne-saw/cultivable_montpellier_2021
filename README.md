# cultivable_montpellier_2021

Study of the effects of bacterial treatments on rice infected by Meloidogyne graminicola (Masson et al., unpublished) Data: in planta and in vitro assays with endophytes Analyses: statistical tests on inoculation effects, PCA, heatmap and graphs
